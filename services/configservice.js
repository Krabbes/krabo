'use strict';

var fs = require('fs');

exports.read = function read() {
    let rawdata = fs.readFileSync('services/config.json');
    let config = JSON.parse(rawdata);
    return config;
}

exports.write = function (path, id) {
    let config = this.read();
    config.paths[id].path = path;


    let data = JSON.stringify(config);
    fs.writeFile('services/config.json', data, (err) => {
        if (err) throw err;
        console.log('Data written to file');
    });
}
