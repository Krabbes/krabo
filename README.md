kRABO is a simple web-application with the purpose to create a possibility to serve multi local folderpath at one domain for all origins.

After you clone the project use npm i at project root folder level.

Start kRABO with npm run start.

kRABO will run at port 3000.

Use your favourite browser and navigate to: localhost:3000

Paste your local folderpath in one of the input fields and save.

Now you can request as an example:
  
  localhost:3000/mock/data/test.txt
  
  /data/test.txt must be within the folder you set in one of the privous steps.
 

Info:
Use the code as you like, I don't take any responsibility for misuse.