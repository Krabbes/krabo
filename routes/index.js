var express = require('express');
var router = express.Router();
var cors = require('cors'); //https://www.npmjs.com/package/cors
var configservice = require('../services/configservice');

/* GET home page. */
router.get('/', function (req, res, next) {
  let config = configservice.read();
  res.render('index', {configs:config.paths});
});

router.post('/save', (req, res) => {
  let path = req.body.path;
  let id = req.body.id;
  configservice.write(path, id);
  res.redirect('/');
})

router.get('/mock/*', cors(), (req, res) => {
  let config = configservice.read();
  let path = config.paths[0].path;
  res.sendFile(req.params[0], { root: path });
});

router.get('/fake/*', cors(), (req, res) => {
  let config = configservice.read();
  let path = config.paths[1].path;
  res.sendFile(req.params[0], { root: path });
});

router.get('/dummy/*', cors(), (req, res) => {
  let config = configservice.read();
  let path = config.paths[2].path;
  res.sendFile(req.params[0], { root: path });
});


module.exports = router;
